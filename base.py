#-*- coding: utf-8 -*-
import sqlite3
import json
from scraping import scrape

def start_scrap():
    connection = sqlite3.connect('data.db')

    cursor = connection.cursor()
    cursor.execute('PRAGMA encoding="UTF-8";')
    create_table =("CREATE TABLE IF NOT EXISTS planets (id varchar(3), data json)")
    cursor.execute(create_table)

    for planet in scrape()['data']:
        cursor.execute("insert into planets values (?, ?)", [planet['id'], json.dumps(planet)])

    connection.commit()
    connection.close()

def fetch_data(planet_id):
    connection = sqlite3.connect('data.db')
    cur = connection.cursor()
    cur.execute('PRAGMA encoding="UTF-8";')
    res = cur.execute('select * from planets where planets.id =' + str(planet_id))
    json_doc = res.fetchall()
    connection.commit()
    connection.close()
    return json_doc[0]