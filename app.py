#-*- coding: utf-8 -*-
import sqlite3
from flask import Flask, jsonify
from base import start_scrap, fetch_data
import json

app = Flask(__name__)

def setup_app(app):
    start_scrap()
setup_app(app)

# def routes 
@app.route('/planets/<int:planet_id>')
def index(planet_id):
    return jsonify({'response': fetch_data(planet_id),
    'charset': 'utf-8'})


# run server
if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0', port=5000)
