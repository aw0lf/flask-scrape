#-*- coding: utf-8 -*-
from bs4 import BeautifulSoup
import requests
import re

def scrape():
    l = []
    planets = ['merkury', 'wenus', 'ziemia', 'mars', 'jowisz', 'saturn', 'uran', 'neptun']
    for planet in planets:
        d = {}
        titles = []
        descriptions = []

        d['id'] = planets.index(planet)
        #iteracja po wszystkich pagach strony i parsowanie zawartosci
        base_url = 'https://odkrywcyplanet.pl/' + planet
        d['planet_name'] = planet.title()
        r = requests.get(base_url)
        soup = BeautifulSoup(r.text, "html.parser")
        
        #zawartosc opisow planet wraz z tytulami
        main_content = soup.find('h2')
        titles.append(main_content.get_text())
        next_siblings = main_content.findNextSiblings('h2')
        for sib in next_siblings:
            titles.append(sib.get_text())
        next_sib_description = main_content.findNextSiblings('p')
        for p in next_sib_description:
            if len(p.get_text()) > 0:
                descriptions.append(p.get_text().replace('\n', ' '))
        d['main_content'] = {}
        d['main_content']['titles'] = titles
        d['main_content']['descriptions'] = descriptions

        # zdjecia danej planety
        images = soup.find_all('img', class_="size-full")
        images_src = []
        for image in images:
            image_src = image.get('src')
            images_src.append(image_src)
        d['images_src'] = images_src[:-1]

        #pobieranie opisu wstepnego planety
        page_content = soup.find_all('div', class_="entry-content uk-clearfix")
        for item in page_content:
            short_description = item.findChild('p').get_text()
            d['short_description'] = short_description
            l.append(d)
    return {'data' : l}

if __name__ == "__main__":
        print(scrape())
        